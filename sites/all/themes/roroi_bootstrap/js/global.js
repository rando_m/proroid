jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }

})();

(function ($) {
    $(document).ready(function(){
        $(".exzoom_img_box ul").addClass('exzoom_img_ul');
        var width=$(document).width();
        jQuery('.tb-megamenu-submenu').css({ 'width':width});
        $('.tb-megamenu-item.level-1').each(function(index) {
            var position= $(this).offset().left;
            $(this).find('.tb-megamenu-submenu').css({ 'left':-position });
            var level_2=$(this).find('.level-2');
            $.each(level_2,function(index){
                var position_2= $(this).offset().left;
                $(this).find('.tb-megamenu-submenu').css({ 'left':-position_2 });
            });
        });
        if($('.hero_image').length){
            $('.hero_image').slick({
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
            });
            $('.single-slider').each(function(){
                var img = $(this).data('img');
                $(this).parent().parent().parent().css('background-image','url("'+img+'")');
            })
        }
        $('.tb-megamenu-button').on('click',function(){
            $('header').toggleClass('mobile-header');
            $('html').toggleClass('mobile-html');
        });
        // $(document).on('resize',function(){
        //     var width = $(document).width();
        //     if(width > 767){
        //         $('header').removeClass('mobile-header');
        //     }
        // })
        $(document).on('click','.open .tb-megamenu-clicked',function(e){
            $(this).addClass('mobile-sub-menu-clicked');
        });
        $(document).on('click','.mobile-sub-menu-clicked',function(e){
            e.preventDefault();
            $(this).parent().removeClass('open');
            $(this).removeClass('mobile-sub-menu-clicked');
            $(this).removeClass('tb-megamenu-clicked');
        });
        // if($('#webform-client-form-4').length){
        //     $('#webform-client-form-4 label .form-required').html();
        // }

      $('#footer .expanded').removeClass('dropdown');
      $('#footer .expanded ul ').removeClass('dropdown-menu').addClass('leaf-block');
    });



})(jQuery);


// (function($){
//     $(document).ready(function(){
//         $(".node-type-product #exzoom").exzoom({
//         "navWidth": 60,
//         "navHeight": 60,
//         "navItemNum": 5,
//         "navItemMargin": 7,
//         "navBorder": 1,
//         "autoPlay": false
//     });
//
//     });
// })(jQuery);


