<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<?php
$is_on_sale = false;
if(isset($node->list_price) && $node->list_price != '' && isset($node->sell_price) && $node->sell_price !=''){
  $list_price = (float)$node->list_price;
  $sell_price = (float)$node->sell_price;
  if($list_price > $sell_price){
    $save = number_format($list_price - $sell_price, 2);
    $is_on_sale =true;

  }
}
?>
<div class="container">
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="content clearfix"<?php print $content_attributes; ?>>
      <div class="row single-product-page">
          <div class="col-sm-6 images exzoom" id="exzoom">
              <div class="row exzoom_img_box">
                  <ul class='exzoom_img_ul'>
                    <?php foreach($content['uc_product_image']['#items'] as $img): ?>
                    <li>
                      <img src="<?php print file_create_url($img['uri']);?>">
                    </li>
                    <?php endforeach; ?>
              </div>
              <div class="exzoom_nav"></div>

          </div>
          <div class="col-sm-6">
              <div class="row">
                  <div class="col-sm-12 general">
                      <?php if($is_on_sale){
                        echo '<p class="on-sale"><span>Now on sale</span></p>';
                      }
                      ?>
                      <h1 class="product-title"><?php print $title; ?> <span class="sku">SKU: <?php echo $node->model; ?></span></h1>
                      <?php if(isset($node->field_product_country_of_origin)){
                        print render($content['field_product_country_of_origin']);
                      }
                      ?>
                    <div class="total-price"><div class="product-info sell-price"><div class="uc-price<?php  if($is_on_sale){ echo ' discounted'; } ?>"><sup>$</sup><?php echo (float)$node->sell_price; ?></div></div><?php if(isset($node->field_product_unit[LANGUAGE_NONE][0]['value']) && $node->field_product_unit[LANGUAGE_NONE][0]['value'] != ''){ ?><div class="product-unit"><span> / <?php echo $node->field_product_unit[LANGUAGE_NONE][0]['value']; ?></span></div><?php } ?></div>
                      <?php if($is_on_sale){
                         echo "<p class='discount'>Was $".$list_price." <span>You save $".$save."</span></p>";
                      }
                      ?>
                    <?php if(isset($node->field_product_discount)){
                      print render($content['field_product_discount']);
                    }
                    ?>
                      <p class="stock-level">
                      <?php
                      $stock = uc_stock_level($node->model);
                      if ($stock > 0) {
                        print '<img src="/sites/all/themes/roroi_bootstrap/images/tick-stock.svg"><span class="in-stock"> In stock</span>';
                      } else {
                        print '<img src="/sites/all/themes/roroi_bootstrap/images/no-stock.svg"><span class="out-stock"> Out of stock</span>';
                      }
                      ?>
                      </p>

                      <?php
                      if($stock > 0): print render($content['add_to_cart']); endif; ?>
                  </div>
              </div>
          </div>
        <?php if($body = render($content['body'])):
        ?>
          <div class="col-sm-12 tab-details">
              <div class="tabbable">
                  <ul class="nav nav-pills nav-stacked col-md-3">
                      <li class="active"><a href="#a" data-toggle="tab">Product Description</a></li>
                      <!--<li><a href="#b" data-toggle="tab">Discounts</a></li>-->
                      <!--<li><a href="#c" data-toggle="tab">Ingredients</a></li>-->
                  </ul>
                  <div class="tab-content col-md-9">
                      <div class="tab-pane active" id="a"><?php print $body; ?></div>
                      <!--<div class="tab-pane" id="b">Discounts</div>-->
                      <!--<div class="tab-pane" id="c"><?php //print $node->field_product_ingredients[UND][0]['value']; ?> </div>-->
                  </div>
              </div>
          </div>
        <?php endif; ?>
      </div>
  </div>
</div>
</div>
<script type="text/javascript">
  (function($) {
    jQuery(document).ready(function(e){
      if($(".node-type-product #exzoom").length){
      $(".node-type-product #exzoom").exzoom({
        "navWidth": 60,
        "navHeight": 60,
        "navItemNum": 5,
        "navItemMargin": 7,
        "navBorder": 1,
        "autoPlay": false
      });
      }
    });
  })(jQuery);
</script>
