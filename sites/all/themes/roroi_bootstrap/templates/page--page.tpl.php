<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup templates
 */global $path;
?>
<div id="page-wrapper">
  <div id="page" class="container-fluid page">
    <?php print render($page['leaderboard']); ?>
    <header id="header" class="clearfix" role="banner" >
      <div class="region region-header">
        <div class="region-inner clearfix">
            <div class="container">
                <div class="row">
                    <div id="block-delta-blocks-logo" class="block block-delta-blocks col-sm-3">
                        <div class="logo-img">
                            <a href="/" class="active"><img class="site-logo" typeof="foaf:Image" src="<?php echo $path; ?>/sites/all/themes/roroi_bootstrap/images/logo.svg"><img class="site-logo site-logo-mobile" style="display:none;" typeof="foaf:Image" src="<?php echo $path; ?>/sites/all/themes/roroi_bootstrap/images/logo-mobile.svg"></a>
                        </div>
                    </div>
                    <div id="block-search-form" class="block block-search col-sm-6" role="search">
                        <?php
                        $block = module_invoke('search', 'block_view', 'form');
                        print render($block['content']);
                        ?>
                    </div>
                    <div class="col-sm-3 custom-blocks">
                        <div class="account-block col-sm-7 text-right">
                            <?php $block = module_invoke('roroi', 'block_view', 'login_block');
                            print render($block['content']);
                            ?>
                        </div>
                        <div class="cart-block col-sm-5 text-left">
                            <?php $block = module_invoke('roroi', 'block_view', 'microcart');
                            print render($block['content']);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <nav id="block-system-main-menu" role="navigation">
                        <?php $block = module_invoke('tb_megamenu', 'block_view', 'main-menu');
                        print render($block['content']);
                        ?>
                    </nav>
                    <div id="block-block-5" class="block custom-show-menu">
                        <div class="block-inner">
                            <div class="mobile-menu-button">
                                <a href="#">&nbsp;</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </header>

    <?php print render($page['menu_bar']); ?>

    <?php print $messages; ?>
    <?php print render($page['help']); ?>

    <?php print render($page['secondary_content']); ?>

    <main id="content-column" class="content-column" role="main">
          <div class="container">
            <div class="row">
              <?php if($sidebar_first = render($page['sidebar_first'])){
                ?>
                <div class="col-sm-3">
                  <?php print $sidebar_first; ?>
                </div>
                <?php
              }
              ?>
              <?php if($sidebar_first){
                echo '<div class="content-inner col-sm-9">';
              }
              else{
                echo '<div class="content-inner col-sm-12">';
              }
              ?>
              <?php print render($page['highlighted']); ?>

              <div id="main-content">

                <?php print render($title_prefix); ?>
                <?php if (!empty($title)): ?>
                  <h1 class="page-header"><?php print $title; ?></h1>
                <?php endif; ?>
                <?php print render($title_suffix); ?>
                <?php if ($action_links = render($action_links)): ?>
                  <header>
                    <?php if ( $action_links): ?>
                      <div id="tasks">
                        <?php if ($action_links = render($action_links)): ?>
                          <ul class="action-links clearfix"><?php print $action_links; ?></ul>
                        <?php endif; ?>
                      </div>
                    <?php endif; ?>

                  </header>
                <?php endif; ?>
                <?php if (!empty($tabs)): ?>
                  <?php print render($tabs); ?>
                <?php endif; ?>
                <?php if ($content = render($page['content'])): ?>
                  <div id="content" class="region">
                    <?php print $content; ?>
                  </div>
                <?php endif; ?>

                <?php print $feed_icons; ?>

                <?php print render($title_suffix); ?>

                <div class="content-aside">
                  <?php print render($page['content_aside']); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
  </main>
      <?php $sidebar_second = render($page['sidebar_second']); print $sidebar_second; ?>

    </div>

    <?php print render($page['tertiary_content']); ?>

    <?php if ($page['footer']): ?>
      <footer id="footer" class="footer container-fluid">
        <?php print render($page['footer']); ?>
      </footer>
    <?php endif; ?>

  </div>
</div>
