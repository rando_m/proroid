<div id="wrapper" dir="ltr" style="background-color: #f5f5f5; margin: 0; padding: 70px 0 70px 0; -webkit-text-size-adjust: none !important; width: 100%;">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tr>
<td align="center" valign="top">
						<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container" style="box-shadow: 0 1px 4px rgba(0,0,0,0.1) !important; background-color: #fdfdfd; border: 1px solid #dcdcdc; border-radius: 3px !important;">
<tr>
<td align="center" valign="top">
						<img src="https://www.proroid.com/assets/email-header-17.jpg" alt="Your Proroid Order" />			
<!-- End Header -->
</td>
							</tr>
<tr>
<td align="center" valign="top">
									<!-- Body -->
									<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body"><tr>
<td valign="top" id="body_content" style="background-color: #fdfdfd;">
												<!-- Content -->
												<table border="0" cellpadding="20" cellspacing="0" width="100%"><tr>
<td valign="top" style="padding: 48px;">
															<div id="body_content_inner" style='color: #737373; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;'>

 <h1 style='color: #222222; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 30px; font-weight: 300; line-height: 150%; margin: 0; text-align: left; text-shadow: 0 1px 0 #335984; -webkit-font-smoothing: antialiased;'>New order</h1>
 <p style="margin: 0 0 16px;"><?php print t('Thank you for your order, !order_first_name!', array('!order_first_name' => $order_first_name)); ?></p>
 <?php if (isset($order->data['new_user'])): ?>
            <p><b><?php print t('An account has been created for you with the following details:'); ?></b></p>
            <p><b><?php print t('Username:'); ?></b> <?php print $order_email; ?><br />
            <b><?php print t('Password:'); ?></b> <?php print $order_new_password; ?></p>
            <p>
              <?php endif; ?>
              </p>
            

<?php


function roundUpToAny($n,$x=5) {
    return (ceil($n)%$x === 0) ? ceil($n) : round(($n+$x/2)/$x)*$x;
}
?>
<p><strong>Cash Payment</strong><br /> 
<p>Please mail <strong>Canadian Dollars in the exact amount of <?php print roundUpToAny(round(($order->order_total),0)); ?> (rounded up to the nearest 5 so you don't send coins)</strong>. </p>
<p>Mail via <strong>Xpress Post or any other Courier</strong> to:</p>
<p><strong>Dave Simard<br />
  3 - 1750 The Queensway<br />
  Suite # 425<br />
  Toronto, Ontario<br />
  M9C 5H5<br />
  Canada</strong>
 </p>
<p><strong>Hide funds</strong> as best you can (use CD-cases, birthday cards, magazines, etc). Write ONLY your email address inside and NOTHING ELSE!<br />
Your order will ship next business day after receiving funds.</p>

<p><strong>Once payment is sent:</strong> </p>
<ul>
  <li>Log into your account. </li>
  <li>Click on &quot;Orders&quot;</li>
  <li>Click the order number you're paying for.</li>
  <li>Enter the tracking number and submit.</li>
  </ul>
<p>DON'T EMAIL us asking if we received your money. Such emails will be ignored. You can track it online. If it says delivered we've got it. If there's a problem we will email you immediately.</p>
<ul>
<li><u>Do not</u> use Regular mail!</li>
<li>We <u>do not</u> accept Money Orders! Cash Only!</li>
<li>Round up the amount as to avoid sending coins. If you send coins, tape them do so they do not rattle.</li>
<li>Make sure you send the <u>correct amount</u>. Any order with an amount short of the total will to be shipped until you send the full payment. No exceptions!</li>
<li>Payment is to be <u>mailed only</u>, no dropping it off to the address personally, no matter how close you are!</li>
  </li>
</ul>
<strong>WARNING</strong> - disclosing this payment method or address to anyone will result in Immediate Termination of this privilege.
                    
<p>After you have sent your payment, you need to enter your transfer tracking number by visiting your order section here <?php print $order_link; ?>.</p>
                    
            <h2 style='color: #002f65; display: block; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; margin: 16px 0 8px; text-align: left;'>
              Order
                #<?php print $order_link; ?> (<time datetime="<?php print $order_created; ?>"><?php print $order_created; ?></time>)</h2>

<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; color: #737373; border: 1px solid #e4e4e4;" border="1">
<thead><tr>
<th class="td" scope="col" style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">Product</th>
			<th class="td" scope="col" style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">Quantity</th>
			<th class="td" scope="col" style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">Price</th>
		</tr></thead>
<tbody>
 <?php foreach ($products as $product): ?>
<tr class="order_item">
<td class="td" style="text-align: left; vertical-align: middle; border: 1px solid #eee; word-wrap: break-word; color: #737373; padding: 12px;"><?php print $product->title; ?> <?php print $product->individual_price; ?><br /><?php print $product->details; ?></td>
			<td class="td" style="text-align: left; vertical-align: middle; border: 1px solid #eee; color: #737373; padding: 12px;"><?php print $product->qty; ?></td>
			<td class="td" style="text-align: left; vertical-align: middle; border: 1px solid #eee; color: #737373; padding: 12px;"><span class="woocommerce-Price-amount
amount"><?php print $product->total_price; ?></span></td>
		</tr>
         <?php endforeach; ?></tbody>
<tfoot>
<tr>
  <th class="td" scope="row" colspan="2" style="text-align: left; border-top-width: 4px; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">Subtotal:</th>
  <td class="td" style="text-align: left; border-top-width: 4px; color: #737373; border: 1px solid #e4e4e4; padding: 12px;"><span class="woocommerce-Price-amount amount"><?php print $order_subtotal; ?></span></td>
</tr>
<?php foreach ($line_items as $item): ?>
                    <?php if ($item['type'] == 'subtotal' || $item['type'] == 'total')  continue; ?>

                    <tr>
                      <th class="td" scope="row" colspan="2" style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">
                        <?php print $item['title']; ?>:
                      </th>
                     <td class="td" style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">
                        <?php print $item['formatted_amount']; ?>
                      </td>
                    </tr>

                    <?php endforeach; ?>

<tr>
<th class="td" scope="row" colspan="2" style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">Total:</th>
						<td class="td" style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;"><span class="woocommerce-Price-amount amount"><?php print $order_total; ?></span></td>
					</tr>				

<tr>
<th class="td" scope="row" colspan="2" style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;">Total to send (rounded):</th>
						<td class="td" style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;"><span class="woocommerce-Price-amount amount">CAD $<?php print roundUpToAny(round(($order->order_total),0)); ?></span></td>
					</tr>	

				
</tfoot>
</table>	
<h2 style='color: #002f65; display: block; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 18px; font-weight: bold; line-height: 130%; margin: 16px 0 8px; text-align: left;'>Customer details</h2>
	<ul>
<li>
<strong>Email address:</strong> <span class="text" style='color: #505050; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;'><?php print $order_email; ?></span></li>
			</ul>
<table id="addresses" cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top;" border="0"><tr>
<td class="td" style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;" valign="top" width="50%">
			<h3 style='color: #002f65; display: block; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 16px; font-weight: bold; line-height: 130%; margin: 16px 0 8px; text-align: left;'>Shipping address</h3>

			<p class="text" style='color: #505050; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; margin: 0 0 16px;'><?php print $order_shipping_address; ?></p>
		</td>
					<td class="td" style="text-align: left; color: #737373; border: 1px solid #e4e4e4; padding: 12px;" valign="top" width="50%">
				<h3 style='color: #002f65; display: block; font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-size: 16px; font-weight: bold; line-height: 130%; margin: 16px 0 8px; text-align: left;'>&nbsp;</h3>
			</td>
			</tr></table>
</div>
														</td>
													</tr></table>
<!-- End Content -->
</td>
										</tr></table>
<!-- End Body -->
</td>
							</tr>
<tr>
<td align="center" valign="top">
									<!-- Footer -->
									<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer"><tr>
<td valign="top" style="padding: 0; -webkit-border-radius: 6px;">
												<table border="0" cellpadding="10" cellspacing="0" width="100%"><tr>
<td colspan="2" valign="middle" id="credit" style="padding: 0 48px 48px 48px; -webkit-border-radius: 6px; border: 0; color: #6682a3; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;">
															<p>Proroid - Your number one shop in Canada</p>
														</td>
													</tr></table>
</td>
										</tr></table>
<!-- End Footer -->
</td>
							</tr>
</table>
</td>
				</tr></table>
</div>