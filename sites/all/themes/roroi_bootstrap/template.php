<?php

/**
 * @file
 * Bootstrap sub-theme.
 *
 * Place your custom PHP code in this file.
 */

function roroi_bootstrap_preprocess_page(&$variables) {
  if (isset($variables['node']->type)) {
    $nodetype = $variables['node']->type;
    $variables['theme_hook_suggestions'][] = 'page__' . $nodetype;
  }
  if (arg(0) == 'taxonomy') {
    $variables['theme_hook_suggestions'][] = 'page__taxonomy';
  }
  /* redirecting users to orders */
  if(isset($variables['node']) && $variables['node']->nid == 525){
    global $user;
    if(user_is_logged_in()){
      drupal_goto('user/'.$user->uid.'/orders');
    }
    else{
      drupal_goto('user/login');
    }
  }
  drupal_add_js("(function(h,o,t,j,a,r){
       h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
       h._hjSettings={hjid:1076618,hjsv:6};
       a=o.getElementsByTagName('head')[0];
       r=o.createElement('script');r.async=1;
       r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
       a.appendChild(r);
   })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');", array(
    'type' => 'inline',
    'scope' => 'header',
    'weight' => 5,
  ));

}
/**
 * hook_form_FORM_ID_alter
 */
function roroi_bootstrap_form_search_block_form_alter(&$form, &$form_state, $form_id) {
//  $form['search_block_form']['#title'] = t('Search'); // Change the text on the label element
//  $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
//  $form['search_block_form']['#size'] = 40;  // define size of the textfield
//  $form['search_block_form']['#default_value'] = t('Search'); // Set a default value for the textfield
//  $form['actions']['submit']['#value'] = t('GO!'); // Change the text on the submit button
//  $form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/images/search-button.png');
//
//  // Add extra attributes to the text box
//  $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Search';}";
//  $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'Search') {this.value = '';}";
//  // Prevent user from searching the default text
//  $form['#attributes']['onsubmit'] = "if(this.search_block_form.value=='Search'){ alert('Please enter a search'); return false; }";

  // Alternative (HTML5) placeholder attribute instead of using the javascript
  $form['search_block_form']['#attributes']['placeholder'] = t('Search Products');
}
