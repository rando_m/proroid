<?php

/**
 * @file
 * Payment pack administration menu items.
 *
 */

/**
 * Receive a Cash Payment Transfer for an order.
 */
function uc_cp_receive_cp_form($form, $form_state, $order) {
  $balance = uc_payment_balance($order);
  
  $form['balance'] = array('#markup' => uc_currency_format($balance));
  $form['order_id'] = array(
    '#type' => 'hidden',
    '#value' => $order->order_id,
  );
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => uc_currency_format($balance, FALSE, FALSE, '.'),
    '#size' => 10,
    '#field_prefix' => variable_get('uc_sign_after_amount', FALSE) ? '' : variable_get('uc_currency_sign', '$'),
    '#field_suffix' => variable_get('uc_sign_after_amount', FALSE) ? variable_get('uc_currency_sign', '$') : '',
  );
  $form['comment'] = array(
    '#type' => 'textfield',
    '#title' => t('Receipt No. / Comment'),
    '#description' => t('Any notes about the Cash Payment Transfer, like receipt number.'),
    '#size' => 64,
    '#maxlength' => 256,
  );
 
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Receive Cash Payment Transfer'),
  );

  return $form;
}



function uc_cp_receive_cp_form_submit($form, &$form_state) {
  global $user;

  uc_payment_enter($form_state['values']['order_id'], 'cp',
                  $form_state['values']['amount'], $user->uid, '', $form_state['values']['comment']);
                  
  drupal_goto('admin/store/orders/' . $form_state['values']['order_id']);
}
