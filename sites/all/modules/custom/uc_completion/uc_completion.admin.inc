<?php

/**
 * @file
 * Payment pack administration menu items.
 *
 */

/**
 * Receive a Transfer for an order.
 */
function uc_completion_complete_form($form, $form_state, $order) {
 
  $form['order_id'] = array(
    '#type' => 'hidden',
    '#value' => $order->order_id,
  );
  $form['tracking_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Tracking Number for order ') . $order->order_id,
    '#size' => 10,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit and complete order'),
  );

  return $form;
}


/**
 * @param $form
 * @param $form_state
 */
function uc_completion_complete_form_submit($form, &$form_state) {
  				global $user;
				module_load_include('module', 'uc_order', 'uc_order.module');
				
				$orders = uc_order_load($form_state['values']['order_id'], $reset = FALSE);
 				$orders->field_tracking_number["und"][0]["value"] =  $form_state['values']['tracking_number'];
 				uc_order_save($orders);
        uc_order_update_status($form_state['values']['order_id'], 'completed');
        uc_order_comment_save($form_state['values']['order_id'], $user->uid, 'Order has been shipped. Tracking number: '.$form_state['values']['tracking_number'], 'order','completed');
        drupal_goto('admin/store/orders/' . $form_state['values']['order_id']);
}
