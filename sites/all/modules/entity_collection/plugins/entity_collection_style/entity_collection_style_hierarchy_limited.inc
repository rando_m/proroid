<?php
/**
 * @file
 * A hiearchy style plugin.
 */

/**
 * This style can show groups of content in hierarchies.
 */
class EntityCollectionStyleHierarchyLimited extends EntityCollectionStyleHierarchy {

  /**
   * Build the hierarchy.
   * @see EntityCollectionStyle::build().
   */
  public function build(EntityCollection $entity, EntityCollectionTreeNode &$tree, $langcode = NULL, $settings = array()) {
    $this->settings = array_merge($this->settings, $settings);
    $entity->settings['style_settings'] = $this->settings;

    // This will be used to remember if a part of the tree has already been rendered.
    if (!isset($tree->processed_offset)) {
      $tree->processed_offset = 0;
    }
    
    // If a breakpoint si enabled splice the tree as first thing.
    if ( $this->useBreakpoint() ) {
      $tree = $tree->splice(0, $this->getBreakpoint($entity));
    }

    $limitation = $this->getLimits();
    if (!empty($limitation)) {
      if ($limitation['offset']) {
        $tree->processed_offset += $limitation['offset'];
      }

      /**
       * Fix to ensure that we show the correct number of items
       * even if some of them are not published.
       */
      // Set a counter for the number of items that will be shown.
      $visible_items = 0;
      // Set a counter for the number of items that have been processed.
      $processed_items = 0;

      foreach ($tree->children as $value) {
        if (entity_access('view', $value->type, $value->content)) {
          // If the current user can view the content, increase the counter.
          $visible_items++;
        }
        // Always increase the number of processed items.
        $processed_items++;
        // Exit from the loop when we have reached the number of required items.
        if ($limitation['length'] > 0 && $visible_items >= $limitation['length']) {
          break;
        }
      }
      // Store the updated number of items that should be processed for display.
      $limitation['length'] = $processed_items;

      $limited_tree = clone $tree;
      $limited_tree->splice($tree->processed_offset, $limitation['length']);
      $tree->processed_offset += $limitation['length'];

      return parent::build($entity, $limited_tree, $langcode);
    }

    return parent::build($entity, $tree, $langcode);
  }

  /**
   * Render the content of a tree as a tree using a template.
   */
  protected function renderTreeNode(&$content, $entity, EntityCollectionTreeNode $item, $langcode = NULL) {
    $element = array(
      '#collection_children' => array(),
      '#weight' => $item->position,
      '#theme' => 'entity_collection_style_hierarchy'
    );
    // Render the item only if the user has the view permission.
    if (isset($item->content) && entity_access('view', $item->type, $item->content) !== FALSE) {
      $data = EntityCollectionRow::getRow($entity)->build($entity, $item, $langcode);
      $element['#collection_parent'] = $data;
    }
    foreach ($item->getChildren() as $child) {
      $this->renderTreeNode($element['#collection_children'], $entity, $child, $langcode);
    }
    if (isset($element['#collection_children'][0]) || isset($element['#collection_parent'])) {
      $content[] = $element;
    }
  }

  /**
   * Adds the required settings
   */
  public function settingsForm(&$form, $settings = array()) {
    $settings = $settings + $this->settings;
    parent::settingsForm($form, $settings);

    $limits = !empty($settings['limitation']) ? $settings['limitation'] : array('length'=>0,'offset'=>0);

    $form['limit'] = array(
      '#id' => 'edit-settings-style-limit',
      '#type' => 'checkbox',
      '#title' => t('Display a specified number of items'),
      '#default_value' => ($limits['offset']+$limits['length'] != 0),
    );

    $form['limitation'] = array(
      '#input' => TRUE,
      '#type' => 'fieldset',
      '#states' => array(
        'invisible' => array(
          ':input[id="edit-settings-style-limit"]' => array('checked' => FALSE),
        ),
      ),
    );

    $form['limitation']['length'] = array(
      '#type' => 'textfield',
      '#title' => t('Items to display'),
      '#description' => t('The number of items to display. Enter 0 for no limit.'),
      '#default_value' => $limits['length'],
    );

    $form['limitation']['offset'] = array(
      '#type' => 'textfield',
      '#title' => t('Offset'),
      '#description' => t('The number of items to skip. For example, if this field is 3, the first 3 items will be skipped and not displayed.'),
      '#default_value' => $limits['offset'],
    );

    $form['enable_breakpoint'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Breakpoint'),
      '#description' => t('Enable/Disable the break functionality. If disabled this will behave like a Hierarchy style.'),
      '#default_value' => !empty($settings['enable_breakpoint']),
    );
  }

  public function getBreakpoint(EntityCollection $collection) {
    return EntityCollectionStorage::getBundleStorage($collection->bundle)
      ->getBreakpoint($collection, $collection->contexts);
  }
  
  public function useBreakpoint(){
    return !empty($this->settings['enable_breakpoint']);
  }
  
  public function setBreakpointStatus($enabled = TRUE) {
    $this->settings['enable_breakpoint'] = $enabled;
  }

  public function toggleBreakpointStatus($enabled = TRUE) {
    $this->setBreakpointStatus( !$this->useBreakpoint() );
  }

  public function getLimits() {
    return isset($this->settings['limitation']) ? $this->settings['limitation'] : array('length'=>0,'offset'=>0);
  }

}
