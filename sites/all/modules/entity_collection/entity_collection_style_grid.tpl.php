<?php
/**
 * @file
 * Template for an entity collection.
 */
?>
<div class="item <?php print $classes; ?>">
	<?php print render($item); ?>
</div>

